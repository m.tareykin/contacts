from django.shortcuts import render

def getIndex(request):
  template = 'index.pug'
  return render(request, template, {})