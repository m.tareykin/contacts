from django.shortcuts import render
from django.http import HttpResponse
import json
from collections import defaultdict

from ..contacts.models import Contact, Region, Town

def deleteComment(request):
  if request.POST:
    comment = request.POST.get('comment-id')
    contact = Contact.objects.get(pk=comment)
    try:
      contact.delete()
      response = {
        'status': 'success',
        'message': 'Контакт успешно добавлен'
      }
    except:
      response = {
        'status': 'error',
        'error': 'Проверьте достоверность данных',
      }
  else:
    response = {
      'status': 'error',
      'error': 'Ошибка',
    }
  return HttpResponse(json.dumps(response))

def getComments(request):
  contacts = Contact.objects.all()
  template = "view.pug"
  return render(request, template, {'contacts': contacts})

def getStat(request):
  template = "stat.pug"
  regions = Region.objects.all()
  stat = []
  for region in regions:
    count_region = 0
    for town in region.towns.all():
      count_region += Contact.objects.filter(town=Town.objects.get(town=town.town)).count()
    if count_region > 4:
      stat_reg = {'name': region.region, 'count': count_region, 'pk': region.pk}
      stat.append(stat_reg)
  return render(request, template, {'stat': stat})


def getStatRegion(request, id):
  template = 'stat_ext.pug'
  region = Region.objects.get(pk=id)
  towns = region.towns.all()
  stat = []
  for town in towns:
    count = Contact.objects.filter(town=town).count()
    stat_town = {'name': town.town, 'count': count}
    stat.append(stat_town)
  return render(request, template, {'stat': stat, 'region': region})


