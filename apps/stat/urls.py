from django.conf.urls import url
from . import views

urlpatterns = [
  url(r'^$', views.getStat, name="get-stat"),
  url(r'^(?P<id>.+)/$', views.getStatRegion, name="get-stat-region"),
]