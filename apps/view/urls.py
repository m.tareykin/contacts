from django.conf.urls import url
from . import views

urlpatterns = [
  url(r'^delete-comment', views.deleteComment, name="delete-comment"),
  url(r'^$', views.getComments, name="get-comments"),
]