from django.shortcuts import render
from django.http import HttpResponse
import json

from ..contacts.models import Contact

def deleteComment(request):
  if request.POST:
    comment = request.POST.get('comment-id')
    contact = Contact.objects.get(pk=comment)
    try:
      contact.delete()
      response = {
        'status': 'success',
        'message': 'Контакт успешно добавлен'
      }
    except:
      response = {
        'status': 'error',
        'error': 'Проверьте достоверность данных',
      }
  else:
    response = {
      'status': 'error',
      'error': 'Ошибка',
    }
  return HttpResponse(json.dumps(response))

def getComments(request):
  contacts = Contact.objects.all()
  template = "view.pug"
  return render(request, template, {'contacts': contacts})