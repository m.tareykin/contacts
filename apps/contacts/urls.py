from django.conf.urls import url
from . import views

urlpatterns = [
  url(r'^get-towns', views.getTowns, name="get-towns"),
  url(r'^save-contact', views.saveContact),
  url(r'^$', views.getContactForm, name="get-contact-form"),
]