# Generated by Django 2.1.4 on 2018-12-25 09:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0002_auto_20181225_0843'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='contact',
            options={'verbose_name': 'Контакт', 'verbose_name_plural': 'Контакты'},
        ),
        migrations.RemoveField(
            model_name='contact',
            name='comment2',
        ),
    ]
