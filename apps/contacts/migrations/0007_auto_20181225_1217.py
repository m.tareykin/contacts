# Generated by Django 2.1.4 on 2018-12-25 12:17

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('contacts', '0006_auto_20181225_1216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='town',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='contacts.Town', verbose_name='Город'),
        ),
    ]
