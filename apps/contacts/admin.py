from django.contrib import admin

from .models import Contact, Region, Town


class ContactAdmin(admin.ModelAdmin):
    pass

class RegionAdmin(admin.ModelAdmin):
    pass

class TownAdmin(admin.ModelAdmin):
    pass


admin.site.register(Contact, ContactAdmin)
admin.site.register(Region, RegionAdmin)
admin.site.register(Town, TownAdmin)