from django.shortcuts import render
from django.http import HttpResponse
from django.template.loader import render_to_string
from django.core.exceptions import ValidationError

import json

from .models import Region, Contact, Town

def getContactForm(request):
  regions = Region.objects.all()
  template = 'contacts.pug'
  return render(request, template, {'regions': regions })


def getTowns(request):
  if request.POST:
    region = request.POST.get('region')
    towns = Region.objects.get(pk=region).towns.all()
    response = {
      'status': 'success',
      'template': render_to_string('towns-select.pug', {'towns': towns,})
    }
    return HttpResponse(json.dumps(response))
  else:
    response = {
      'status': 'error',
      'error': 'Не удалось получить'
    }
    return HttpResponse(json.dumps(response))

def saveContact(request):
  if request.POST:
    first_name = request.POST.get('first_name')
    second_name = request.POST.get('second_name')
    third_name = request.POST.get('third_name', '')
    town = request.POST.get('town', '')
    phone = request.POST.get('phone', '')
    email = request.POST.get('email', '')
    comment = request.POST.get('comment')
    if town:
      contact = Contact(first_name=first_name,
                        second_name=second_name,
                        third_name=third_name,
                        town=Town.objects.get(pk=town),
                        phone=phone,
                        email=email,
                        comment=comment,)
    else:
      contact = Contact(first_name=first_name,
                        second_name=second_name,
                        third_name=third_name,
                        phone=phone,
                        email=email,
                        comment=comment, )
    try:
      contact.save()
      response = {
        'status': 'success',
        'message': 'Контакт успешно добавлен'
      }
    except:
      response = {
        'status': 'error',
        'error': 'Проверьте достоверность данных',
      }
  else:
    response = {
      'status': 'error',
      'error': 'Ошибка',
    }
  return HttpResponse(json.dumps(response))

