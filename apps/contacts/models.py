from django.db import models
from django.utils.translation import ugettext_lazy as _


class Contact(models.Model):
    first_name = models.CharField(_('Имя'), max_length=30)
    second_name = models.CharField(_('Фамилия'), max_length=30)
    third_name = models.CharField(_('Отчество'), max_length=30, blank=True)
    town = models.ForeignKey('Town', on_delete=models.CASCADE, blank=True, null=True, verbose_name=_('Город'))
    phone = models.CharField(_('Телефон'), max_length=16, blank=True)
    email = models.CharField(_('Email'), max_length=128, blank=True)
    comment = models.CharField(_('Комментарий'), max_length=300)

    class Meta:
        verbose_name = 'Контакт'
        verbose_name_plural = 'Контакты'

    def __str__(self):
        return 'ФИО: ' + self.first_name + ' ' + self.second_name

class Region(models.Model):
    region = models.CharField(_('Регион'), max_length=64)
    towns = models.ManyToManyField('Town', blank=False, verbose_name=_('Города'))

    class Meta:
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'

    def __str__(self):
        return 'Регион: ' + self.region

class Town(models.Model):
    town = models.CharField(_('Город'), max_length=64, blank=True, default="town name")

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'

    def __str__(self):
        return 'Город: ' + self.town